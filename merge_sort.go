package mergesort

func doSort(input []int) []int {
	l, r := divide(input)
	if len(l) > 1 {
		l = doSort(l)
	}
	if len(r) > 1 {
		r = doSort(r)
	}
	return mergeSorted(l, r)
}

func divide(in []int) (left []int, right []int) {
	mid := len(in) / 2
	return in[0:mid], in[mid:]
}

// TODO add support for the DESC-sorting as well
func mergeSorted(left []int, right []int) []int {
	var l, r int // 'pointers' to the elements in left/right slices

	n := len(left) + len(right)
	result := make([]int, n)
	for i := 0; i < n; i++ {
		lOk, rOk := l < len(left), r < len(right)
		lWon := (lOk && rOk && left[l] <= right[r]) || (lOk && !rOk)
		if lWon {
			result[i] = left[l]
			l++
			continue
		}
		result[i] = right[r]
		r++
	}
	return result
}
