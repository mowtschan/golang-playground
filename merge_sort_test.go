package mergesort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDivide(t *testing.T) {
	testCases := []struct {
		name      string
		input     []int
		wantLeft  []int
		wantRight []int
	}{
		{"empty input", nil, nil, nil},
		{"odd list", []int{1, 2, 3}, []int{1}, []int{2, 3}},
		{"odd list short", []int{1}, []int{}, []int{1}},
		{"even list", []int{1, 2, 3, 4}, []int{1, 2}, []int{3, 4}},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotLeft, gotRight := divide(tc.input)
			assert.Equal(t, tc.wantLeft, gotLeft)
			assert.Equal(t, tc.wantRight, gotRight)
		})
	}
}

func TestMergeSorted(t *testing.T) {
	testCases := []struct {
		name  string
		left  []int
		right []int
		want  []int
	}{
		{"empty input", nil, nil, []int{}},
		{"size 1", []int{2}, []int{1}, []int{1, 2}},
		{"size 1 and 0", []int{1}, []int{}, []int{1}},
		{"size 0 and 1", []int{}, []int{1}, []int{1}},
		{"size 4", []int{2, 3, 5, 7}, []int{1, 4, 6, 8}, []int{1, 2, 3, 4, 5, 6, 7, 8}},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := mergeSorted(tc.left, tc.right)
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestDoSort(t *testing.T) {
	testCases := []struct {
		name     string
		unsorted []int
		want     []int
	}{
		{"empty input", nil, []int{}},
		{"size 1", []int{1}, []int{1}},
		{"size 2", []int{2, 1}, []int{1, 2}},
		{"size 3", []int{3, 2, 1}, []int{1, 2, 3}},
		{"size 4", []int{3, 2, 4, 1}, []int{1, 2, 3, 4}},
		{"size 8", []int{5, 3, 2, 8, 4, 1, 7, 6}, []int{1, 2, 3, 4, 5, 6, 7, 8}},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := doSort(tc.unsorted)
			assert.Equal(t, tc.want, got)
		})
	}
}
